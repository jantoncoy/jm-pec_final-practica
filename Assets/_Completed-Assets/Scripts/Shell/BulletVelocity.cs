 using UnityEngine;

/// <summary>
/// Indica la velocidad de la bala
/// </summary>
 public class BulletVelocity : MonoBehaviour 
 {
    public float m_LaunchForce = 15f;        // The force given to the shell

	void Start() 
	{
		GetComponent<Rigidbody>().velocity = m_LaunchForce * transform.forward;
	}
	void OnCollisionEnter() 
	{
		Destroy (gameObject);
	}
 }
