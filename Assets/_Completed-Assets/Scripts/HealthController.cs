using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Complete.TankHealth tankHealth = other.GetComponent<Complete.TankHealth>();
            if (!tankHealth)
            {
                Complete.LocalTankHealth localTankHealth = other.GetComponent<Complete.LocalTankHealth>();
                float auxLife = localTankHealth.m_StartingHealth - localTankHealth.m_CurrentHealth;
                localTankHealth.TakeDamage(-auxLife);
                Destroy(this.gameObject, 0);
            }
            else
            {
                float auxLife = tankHealth.m_StartingHealth - tankHealth.m_CurrentHealth;
                tankHealth.TakeDamage(-auxLife);

                Destroy(this.gameObject, 0);
            }
        }
    }
}
