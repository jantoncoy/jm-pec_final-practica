using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayNameManager : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<TMP_InputField>().text= PlayerPrefs.GetString("DisplayName");
    }
}
