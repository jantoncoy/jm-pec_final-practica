using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Contiene las acciones para los botones y menus
/// </summary>
public class Menu : MonoBehaviour
{
    /// <summary>
    /// Evento que contiene la funcion para llamar a cambiarColor del jugador
    /// </summary>
    public static Action<int> cambiarColor;

    /// <summary>
    /// Eventos que unen a un jugador
    /// </summary>
    public static Action unirseEquipoAzul, unirseEquipoVerde;

    /// <summary>
    /// CONFIGURACION DE LA PARTIDA
    /// </summary>
    public static bool partidaLocal = true, partidaTodos = true;
    public static bool fuegoAmigo = false;
    /// <summary>
    /// Lobby 
    /// </summary>
    public static Lobby lobby = null;
    public static bool creator = false;

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void juegoTodosContraTodos()
    {
        //La partida es todos contra todos
        Menu.partidaTodos = true;

        SceneManager.LoadScene("_Complete-Game");
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void Jugar()
    {
        SceneManager.LoadScene("_Complete-Game");
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void UnirseEquipoAzul()
    {
        if (unirseEquipoAzul != null)
            unirseEquipoAzul();
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void UnirseEquipoVerde()
    {
        if(unirseEquipoVerde != null)
            unirseEquipoVerde();
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void juegoEquipos()
    {
        //La partida es en equipos
        Menu.partidaTodos = false;

        if (Menu.partidaLocal)
        {
            lobby.CreateGame();
        }
        creator = true;
        SceneManager.LoadScene("_Complete-Game");
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void jugarLocal()
    {
        //La partida es en local
        Menu.partidaLocal = true;
        SceneManager.LoadScene("ModoJuego");
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void jugarLinea()
    {
        //La partida es en linea
        Menu.partidaLocal = false;
        iniciar();
    }

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void iniciar()
    {
        SceneManager.LoadScene("Lobby");
    }

    /// <summary>
    /// Vuelve al menu de inicio
    /// </summary>
    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// accion cuando se pulsa en salir
    /// </summary>
    public void salir()
    {
        Application.Quit(1);
    }

    /// <summary>
    /// accion cuando se pulsa en un color
    /// </summary>
    public void changeColor(int color)
    {
        Menu.cambiarColor(color);
    }

    /// <summary>
    /// accion cuando se pulsa en la flecha para ocultar y mostrar el panel de configuracion
    /// </summary>
    public void switchPanel()
    {
        ControlPaneles.switchOption();
    }

	/// <summary>
    /// accion cuando se pulsa en Logout
    /// </summary>
    public void OnClickLogout()
    {
		PlayfabController playFabController = FindObjectOfType<PlayfabController>();
		playFabController.OnClickLogout();
    }

    /// <summary>
    /// Llama al evento closeError para cerrar la ventana del error
    /// </summary>
    public void cerrarModalError()
    {
        ModalError.closeError();
    }

    public void activateFriendFire()
    {
        fuegoAmigo= this.GetComponentInParent<Toggle>().isOn;
    }

    public void mostrarPuntuacion()
    {
        LobbyManager.mostrarPuntuacion();
    }

    public void ocultarPuntuacion()
    {
        LobbyManager.ocultarPuntuacion();
    }
}
