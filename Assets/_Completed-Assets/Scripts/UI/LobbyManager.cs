using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class LobbyManager : MonoBehaviour
{
    public static Action mostrarPuntuacion;
    public static Action ocultarPuntuacion;
    public GameObject panelPuntuacion;
    public TextMeshProUGUI InfoText, invidivual, equipo;
    // Start is called before the first frame update
    void Start()
    {
        mostrarPuntuacion = mostrarPanel;
        ocultarPuntuacion = ocultarPanel;
        InfoText.text = "Welcome, "+PlayerPrefs.GetString("DisplayName")+"\n"+ PlayerPrefs.GetString("Email");
        PlayfabController.PFC.getPuntuacionEquipo();
        PlayfabController.PFC.getPuntuacionIndividual();

    }

    // Update is called once per frame
    void Update()
    {
        invidivual.text = PlayfabController.PFC.puntuacionIndividual;
        equipo.text = PlayfabController.PFC.puntuacionEquipo;
    }

    void mostrarPanel()
    {
        panelPuntuacion.SetActive(true);
    }

    void ocultarPanel()
    {
        panelPuntuacion.SetActive(false);
    }
}
