using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;

/// <summary>
/// Clase encargada de gestionar los nombres de los jugadores
/// </summary>
public class UINickname : NetworkBehaviour
{
	[HideInInspector][SyncVar]
    public string m_NicknameText;                      

	private TextMeshProUGUI  m_Input;
	private TMP_InputField  m_InputField;
	public TextMeshPro m_Nickname;

    // Start is called before the first frame update
    void Start()
    {	
		m_Input = GameObject.Find("NicknameText").GetComponent<TextMeshProUGUI >();
		//m_InputField = GameObject.Find("Input Nickname").GetComponent<TMP_InputField>();
		/*if (isLocalPlayer)
		{
			CmdChangeNickname(PlayerPrefs.GetString("DisplayName"));
			Debug.Log("Contempla lo que sera " + m_NicknameText);
			Debug.Log("Esta "+DisplayName.PlayerDisplayName);
			Debug.Log("Pref "+PlayerPrefs.GetString("DisplayName"));
			m_Input.text = PlayerPrefs.GetString("DisplayName");
			//m_InputField.text = m_NicknameText;
		}*/
		
		
		//m_NicknameText = ;
		//m_Input.text = m_NicknameText;

	}
	
    // Update is called once per frame
    /*void Update()
    {
		
		if (isLocalPlayer)
		{
			if(m_Input.text == "" || m_Input.text == null || m_NicknameText == "" || m_NicknameText == null)
			{
				Debug.Log("bravo brabvo");
				CmdChangeNickname(PlayerPrefs.GetString("DisplayName"));
				m_Input.text = m_NicknameText;
				Debug.Log("Contempla lo que sera " + m_NicknameText);
			
			}
			else {
				Debug.Log("aaaaaa " + m_NicknameText);
				CmdChangeNickname(m_Input.text);
				Debug.Log("Contempla lo que sera " + m_NicknameText);
			}
			
		}

		//Change UI
		m_Nickname.text = m_NicknameText;
    }*/

	// Update is called once per frame
	void Update()
	{
		if (isLocalPlayer)
		{
			CmdChangeNickname(m_Input.text);
		}

		//Change UI
		m_Nickname.text = m_NicknameText;
	}

	/// <summary>
	/// Cambia el nombre del jugador en todos los clientes
	/// </summary>
	/// <param name="newNickname">Nuevo nombre</param>
	[Command]
	public void CmdChangeNickname(string newNickname)
	{
		m_NicknameText = newNickname;
	}

}


