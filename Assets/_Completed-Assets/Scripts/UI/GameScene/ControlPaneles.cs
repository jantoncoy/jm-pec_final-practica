using Complete;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
/// <summary>
/// Gestor que se ocupa de reiniciar el nivel y gestionar los paneles
/// </summary>
public class ControlPaneles : NetworkBehaviour
{
    /// <summary>
    /// Panel abieto
    /// </summary>
    public GameObject panelOption;

    /// <summary>
    /// Panel abieto
    /// </summary>
    public GameObject panelResumen;

    /// <summary>
    /// Flecha cerrado
    /// </summary>
    public GameObject flechaOption;

    /// <summary>
    /// Campo texto de victorias
    /// </summary>
    public TextMeshProUGUI victorias;

    /// <summary>
    /// Campo texto de derrotas
    /// </summary>
    public TextMeshProUGUI derrotas;

    /// <summary>
    /// Campos que almacena el numero de victorias y derrotas
    /// </summary>
    public int victoria = 0, derrota = 0;

    /// <summary>
    /// Funcion para solicitar abrir o cerrar el panel
    /// </summary>
    public static Action switchOption;

    /// <summary>
    /// suma puntuacion
    /// </summary>
    public static Action <int,int> sumarPuntuaciones;

    /// <summary>
    /// sube puntuacion
    /// </summary>
    public static Action subePuntuacion;

    /// <summary>
    /// Contiene el jugador local(tank)
    /// </summary>
    public static GameObject miJugador;

    /// <summary>
    /// Contiene todos los jugadores en local
    /// </summary>
    public static List<GameObject> jugadores = new List<GameObject>();

    /// <summary>
    /// Si entra en cuenta regresiva
    /// </summary>
    private bool cuentaRegresiva = false;

    /// <summary>
    /// Marcador de tiempo para que se reinicie
    /// </summary>
    public TextMeshProUGUI cuenta;

    /// <summary>
    /// Tiempo para que se reinicie
    /// </summary>
    private int cuentaNumero;

    /// <summary>
    /// Mensaje de quien ha ganado la ronda
    /// </summary>
    public TextMeshProUGUI mensajeVictoria;

    /// <summary>
    /// Puntuacion
    /// </summary>
    public GameObject puntuacion;
    public TextMeshProUGUI puntuacionText;

    public GameObject teamCanvas;
    public int GreenTeamVic = 0;
    public int BlueTeamVic = 0;

    [SyncVar(hook = "OnChangeChoosingTeams")]
    public bool choosingTeams = false;
    public GameProperties gamePro;
   [SyncVar]
   public bool isTeam = false;
    [SyncVar]
    public bool FriendFire = false;
    // Start is called before the first frame update
    void Start()
    {
        print("Entrando en controlpanelkes");
        //Asignamos el evento
        switchOption = cambiarPanel;
        sumarPuntuaciones = sumarPuntuacion;
        subePuntuacion = enviarPuntuacion;

        if (Menu.partidaLocal)
        {
            panelOption.SetActive(false);
            flechaOption.SetActive(false);
            panelResumen.SetActive(false);
        }
        else
        {
            print("Nia partida local");
            //Agregamos una funcion que se llame cada segundo para mostrar cuenta regresiva
            InvokeRepeating("restaRegresiva", 1, 1);

            //Mostramos el panel resumen cuando iniciamos
            mostrarPanelResumen();
            /*if (Menu.partidaTodos)
            {
                //Agregamos una funcion que se llame cada segundo para mostrar cuenta regresiva
                InvokeRepeating("restaRegresiva", 1, 1);

                //Mostramos el panel resumen cuando iniciamos
                mostrarPanelResumen();
            }
            else
            {
                //panelOption.SetActive(false);
              //  flechaOption.SetActive(false);
            }*/
          
        }
        if (isServer)
        {
            if (!Menu.partidaTodos)
            {
                print("Partida equipos");
                isTeam = true;
                teamCanvas.SetActive(true);
                teamCanvas.GetComponentInChildren<Image>().color = new Color32(255, 0, 255, 100);
                choosingTeams = true;
                GameObject.Find("EnemyManager").GetComponent<EnemyManager>().MAXENEMIGOS = 0;
                if (Menu.fuegoAmigo)
                {
                    FriendFire = true;
                }
            }
        }
        
    }
    // M?todo que actualiza la vida en los clientes en caso de que se produzca un cambio en esta.
    public void OnChangeChoosingTeams(bool oldCT, bool newCT)
    {
        print("Cambiando");
        choosingTeams = newCT;

        // If the current health is at or below zero and it has not yet been registered, call OnDeath
        if (!choosingTeams)
        {
            EnableTankControl();
            teamCanvas.SetActive(false);
           // InvokeRepeating("restaRegresiva", 1, 1);
            //Mostramos el panel resumen cuando iniciamos
           // mostrarPanelResumen();
        }
        if (choosingTeams)
        {
            DisableTankControl();
            teamCanvas.SetActive(true);
            teamCanvas.GetComponentInChildren<Image>().color=new Color32(255,0,255,100);
            teamCanvas.GetComponentInChildren<Text>().text = "Selecciona un Equipo";
            // InvokeRepeating("restaRegresiva", 1, 1);
            //Mostramos el panel resumen cuando iniciamos
            // mostrarPanelResumen();
        }
    }

    int aux = 0;
    // Update is called once per frame
    void Update()
    {
       /* if (aux == 0)
        {
            if (isServer)
            {
                if (gamePro.teamGame)
                {
                    teamCanvas.SetActive(true);
                    choosingTeams = true;
                }
                
            }
            aux = 1;
        }*/


        if (choosingTeams)
        {
            
            DisableTankControl();
            teamCanvas.SetActive(true);
            if (!isServer) return;
            GameObject[] juga;
            juga = GameObject.FindGameObjectsWithTag("Player");
            if (juga.Length < 4)
            {
                return;
            }
            print("Hay 4");
            int greenCoubnt = 0;
            int blueCountDooku = 0;
            for (int i = 0; i < juga.Length; i++)
            {
                if (Menu.partidaLocal)
                {
                    if (juga[i].GetComponent<LocalTankHealth>().Team == -1)
                    {
                        return;
                    }
                    else if (juga[i].GetComponent<LocalTankHealth>().Team == 0)
                    {
                        greenCoubnt++;
                    }
                    else if (juga[i].GetComponent<LocalTankHealth>().Team == 1)
                    {
                        blueCountDooku++;
                    }
                }
                else
                {
                    if (juga[i].GetComponent<TankHealth>().Team == -1)
                    {
                        return;
                    }
                    else if (juga[i].GetComponent<TankHealth>().Team == 0)
                    {
                        greenCoubnt++;
                    }
                    else if (juga[i].GetComponent<TankHealth>().Team == 1)
                    {
                        blueCountDooku++;
                    }
                }
                
            }
            print(blueCountDooku + " - " + greenCoubnt);
            if (blueCountDooku != 2 && greenCoubnt != 2) return;
            //EnableTankControl();
           // teamCanvas.SetActive(false);
            choosingTeams = false;
            //InvokeRepeating("restaRegresiva", 1, 1);
            //Mostramos el panel resumen cuando iniciamos
            //mostrarPanelResumen();
        }
        //Recogemos los jugadores en la partida
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");

        if (!isTeam)
        {
            //Comprobamos numero de jugadores para finalizar la partida
            if (!cuentaRegresiva && jugadoresRestantes.Length == 1)
            {
                //Empezamos cuenta regresiva
                cuentaRegresiva = true;
                cuentaNumero = 14;
                Invoke("final", 14);

            }
        }
        else
        {
            int aux = -1;
            bool win = true;
            foreach (GameObject jugador in jugadoresRestantes)
            {
                //Compprobar tipo de partida           
                if (Menu.partidaLocal)
                {
                    if (aux == -1)
                    {
                        aux = jugador.GetComponent<LocalTankHealth>().Team;
                    }
                    else if (aux != jugador.GetComponent<LocalTankHealth>().Team)
                    {
                        //Dos equipos distintos
                        win = false;
                        break;
                    }
                }
                else
                {
                    if (aux == -1)
                    {
                        aux = jugador.GetComponent<TankHealth>().Team;
                    }
                    else if (aux != jugador.GetComponent<TankHealth>().Team)
                    {
                        //Dos equipos distintos
                        win = false;
                        break;
                    }
                }

            }

            if (!cuentaRegresiva && win)
            {
                //Empezamos cuenta regresiva
                cuentaRegresiva = true;
                cuentaNumero = 14;
                Invoke("final", 14);
            }
        }
        if (isTeam)
        {
            foreach (GameObject jugador in jugadoresRestantes)
            {
                if (Menu.partidaLocal)
                {
                    if (jugador.GetComponent<LocalTankHealth>().Team == 0)
                    {
                        jugador.GetComponent<TankLocalPlayer>().cambiarColor(new Color(0, 1, 0));
                    }
                    else
                    {
                        jugador.GetComponent<TankLocalPlayer>().cambiarColor(new Color(0, 0, 1));
                    }
                }
                else
                {
                    jugador.GetComponent<TankHealth>().friendFire = FriendFire;
                    if (jugador.GetComponent<TankHealth>().Team == 0)
                    {
                        jugador.GetComponent<TankLocalPlayer>().cambiarColor(new Color(0, 1, 0));    
                    }
                    else
                    {
                        jugador.GetComponent<TankLocalPlayer>().cambiarColor(new Color(0, 0, 1));
                    }
                }
                   
            }
        }
       

        
    }

    /// <summary>
    /// Se encarga de calcular y mostrar la cuenta regresiva en el UI
    /// </summary>
    private void restaRegresiva()
    {
        if (cuentaRegresiva)
        {
            if (cuentaNumero > 0)
                cuentaNumero--;

            cuenta.text = "Reinicio en: " + cuentaNumero + "s";
        }
        else
        {
            cuenta.text = "";
        }
    }

    /// <summary>
    /// Activa y programa la ocultacion del panel resumen
    /// </summary>
    private void mostrarPanelResumen()
    {
        //Preparamos panel para que se vea durante un periodo
        activarPanelResumen();
        Invoke("desactivarPanelResumen", 8);
        enviarPuntuacion();
    }

    /// <summary>
    /// Activa el panel resumen
    /// </summary>
    private void activarPanelResumen()
    {
        panelResumen.SetActive(true);
    }

    /// <summary>
    /// Desactiva el panel resumen
    /// </summary>
    private void desactivarPanelResumen()
    {
        panelResumen.SetActive(false);
    }

    /// <summary>
    /// Si esta activo desactiva el panel y viceversa
    /// </summary>
    private void cambiarPanel()
    {
        if (panelOption.activeSelf)
        {
            panelOption.SetActive(false);
            flechaOption.SetActive(true);
        }
        else
        {
            panelOption.SetActive(true);
            flechaOption.SetActive(false);
        }
    }

    /// <summary>
    /// metodo que se llama cuando termine la cuenta regresiva, prepara todo para reiniciar el nivel
    /// </summary>
    public void final()
    {
        if (!isTeam)
        {
            print("Final Todos");
            finalTodos();
        }
        else
        {
            print("Final Equipo");
            finalEquipos();
        }



    }

    public void finalTodos()
    {
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");

        if (jugadoresRestantes.Length > 1)
        {
            //nos salimos porque se ha metido otro jugador
            cuentaRegresiva = false;
            return;
        }
        //Ejecutamos en servidor el reinicio
        if (EnemyManager.finalRonda != null)
            EnemyManager.finalRonda();

        //Si es servidor unicamente no tendra jugador
        if (miJugador != null)
        {
            if (miJugador.activeSelf)
            {
                //Ganamos
                masVictoria();
            }
            else
            {
                //Perdemos
                masDerrota();
            }

            //Actualizamos valores
            victorias.text = victoria + "";
            derrotas.text = derrota + "";

            //Indicamos quien ha ganado
            String nickname = "";
            foreach (GameObject jugador in jugadores)
            {
                if (jugador != null && jugador.activeSelf)
                {
                    //ha ganado
                    nickname = jugador.GetComponent<UINickname>().m_NicknameText;
                }
            }

            if (nickname.Equals(""))
            {
                mensajeVictoria.text = "No hay ganador.";
            }
            else
            {
                mensajeVictoria.text = "El ganador es <b>" + nickname + "</b>";
            }

            //Mostramos panel
            mostrarPanelResumen();
        }

        //Activamos los tanks, le restauramos la vida y lo ponemos donde haya muerto
        foreach (GameObject jugador in jugadores)
        {
            if (jugador != null)
            {
                jugador.SetActive(true);
                TankHealth componenteVida = jugador.GetComponent<TankHealth>();
                componenteVida.m_CurrentHealth = componenteVida.m_StartingHealth;
                componenteVida.OnChangeHealth(componenteVida.m_CurrentHealth, componenteVida.m_StartingHealth);
            }
        }

        cuentaRegresiva = false;
    }
    public void finalEquipos()
    {
        /* GameObject[] jugadoresRestantes;
         jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
         int aux = -1;
         bool win = true;
         foreach (GameObject jugador in jugadoresRestantes)
         {
             //Compprobar tipo de partida           
             if (Menu.partidaLocal)
             {
                 if (aux == -1)
                 {
                     aux = jugador.GetComponent<LocalTankHealth>().TeamId;
                 }
                 else if (aux != jugador.GetComponent<LocalTankHealth>().TeamId)
                 {
                     //Dos equipos distintos
                     win = false;
                     break;
                 }
             }
             else
             {
                 if (aux == -1)
                 {
                     aux = jugador.GetComponent<TankHealth>().Team;
                 }
                 else if (aux != jugador.GetComponent<TankHealth>().Team)
                 {
                     //Dos equipos distintos
                     win = false;
                     break;
                 }
             }

         }*/
        bool win = true;
        if (win)
        {
            int winTeam = -1;
            if (Menu.partidaLocal)
            {
                GameObject[] jugadoresRestantes;
                jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
                 winTeam =jugadoresRestantes[0].GetComponent<LocalTankHealth>().Team;
                if (miJugador.GetComponent<LocalTankHealth>().Team == winTeam)
                {
                    masVictoria();
                }
                else
                {
                    masDerrota();
                }
            }
            else
            {
                GameObject[] jugadoresRestantes;
                jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
                winTeam = jugadoresRestantes[0].GetComponent<TankHealth>().Team;
                if (miJugador.GetComponent<TankHealth>().Team == winTeam)
                {
                    masVictoria();
                }
                else
                {
                    masDerrota();
                }
            }
            print("WinTeam" + winTeam);
            //Actualizamos valores
            victorias.text = victoria + "";
            derrotas.text = derrota + "";

            //Indicamos quien ha ganado
            if (winTeam == 0)
            {
                mensajeVictoria.text = "El ganador es Equipo verde";
            }
            else
            {
                mensajeVictoria.text = "El ganador es Equipo azul";
            }


            //Mostramos panel
            mostrarPanelResumen();
        }

        //Activamos los tanks, le restauramos la vida y lo ponemos donde haya muerto
        foreach (GameObject jugador in jugadores)
        {
            if (jugador != null)
            {
                if (Menu.partidaLocal)
                {
                    jugador.SetActive(true);
                    LocalTankHealth componenteVida = jugador.GetComponent<LocalTankHealth>();
                    componenteVida.m_CurrentHealth = componenteVida.m_StartingHealth;
                    componenteVida.TakeDamage(0);
                    componenteVida.Team=-1;

                }
                else
                {
                    jugador.SetActive(true);
                    TankHealth componenteVida = jugador.GetComponent<TankHealth>();
                    componenteVida.m_CurrentHealth = componenteVida.m_StartingHealth;
                    componenteVida.OnChangeHealth(componenteVida.m_CurrentHealth, componenteVida.m_StartingHealth);
                    componenteVida.changeTeam(-1);
                }
               
            }
        }
        teamCanvas.SetActive(true);
        choosingTeams = true;
        cuentaRegresiva = false;

        DisableTankControl();
        teamCanvas.SetActive(true);
        teamCanvas.GetComponentInChildren<Image>().color = new Color32(255, 0, 255, 100);
        teamCanvas.GetComponentInChildren<Text>().text = "Selecciona un Equipo";
        
    }

    /// <summary>
    /// Suma la puntuacion
    /// </summary>
    /// <param name="puntuacion"></param>
    private void sumarPuntuacion(int puntuacion, int teams)
    {
        if(teams != -1)
        {
            if (miJugador.GetComponent<TankHealth>().TeamId != teams)
                puntuacionText.text = (int.Parse(puntuacionText.text) + puntuacion) + "";
        }
        else
        {
            if (!miJugador.GetComponent<TankHealth>().m_Dead)
                puntuacionText.text = (int.Parse(puntuacionText.text) + puntuacion) + "";
        }

        
    }

    /// <summary>
    /// Enviamos la puntuacion
    /// </summary>
    private void enviarPuntuacion()
    {
        if(PlayfabController.PFC != null)
        PlayfabController.PFC.updatePuntuacion(Menu.partidaTodos,int.Parse(puntuacionText.text));
    }
    
    /// <summary>
    /// Sumamos una derrota
    /// </summary>
    private void masDerrota()
    {
        derrota++;
    }

    /// <summary>
    /// Sumamos una victoria
    /// </summary>
    private void masVictoria()
    {
        victoria++;
    }

    public void EnableTankControl()
    {
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < jugadoresRestantes.Length; i++)
        {
            jugadoresRestantes[i].GetComponent<TankMovement>().enabled=true;
            jugadoresRestantes[i].GetComponent<TankShooting>().enabled = true; 
        }
    }


    public void DisableTankControl()
    {
        GameObject[] jugadoresRestantes;
        jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
        if (jugadoresRestantes.Length == 0) return;
        for (int i = 0; i < jugadoresRestantes.Length; i++)
        {
            jugadoresRestantes[i].GetComponent<TankMovement>().enabled = false;
            jugadoresRestantes[i].GetComponent<TankShooting>().enabled = false;
        }
    }
}
