using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class GestorEquipo : NetworkBehaviour
{

    public readonly SyncList<string> equipoAzul = new SyncList<string>();

    public readonly SyncList<string> equipoVerde = new SyncList<string>();

    public TextMeshProUGUI equiposAzul,equiposVerde;

    // Start is called before the first frame update
    void Start()
    {
        Menu.unirseEquipoAzul = cambiarEquipoAzul;
        Menu.unirseEquipoVerde = cambiarEquipoVerde;
    }

    // Update is called once per frame
    void Update()
    {
        if(equipoAzul.Count > 0)
        {
            string equipos = "";
            foreach (string jugador in equipoAzul)
            {
                equipos += jugador + "\n";
            }
            equiposAzul.text = equipos;
        }


        if (equipoVerde.Count > 0)
        {
            string equipos = "";
            foreach (string jugador in equipoVerde)
            {
                equipos += jugador + "\n";
            }
            equiposVerde.text = equipos;
        }

    }

    [Command]
    void cambiarEquipoAzul()
    {
        equipoAzul.Add(PlayerPrefs.GetString("DisplayName"));
    }

    [Command]
    void cambiarEquipoVerde()
    {
        equipoAzul.Add(PlayerPrefs.GetString("DisplayName"));
    }
}
