using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginManager : MonoBehaviour
{
	private PlayfabController playFabController;

	void Awake()
	{
		playFabController = FindObjectOfType<PlayfabController>();
	}

    // Sets the user's e-mail address
    public void SetUserEmail(string emailIn)
    { 
		playFabController.SetUserEmail(emailIn);
    }

    // Sets the user's password
    public void SetUserPassword(string passwordIn)
    {
		playFabController.SetUserPassword(passwordIn);
    }

    // Sets the user's name
    public void SetUserName(string usernameIn)
    {
       playFabController.SetUserName(usernameIn);
    }

	// Invoked when the login button is clicked
    public void OnClickLogin()
    {
       playFabController.OnClickLogin();
    }
}
