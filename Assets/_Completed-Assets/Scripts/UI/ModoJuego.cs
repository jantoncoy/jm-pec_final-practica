using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class ModoJuego : MonoBehaviour
{
    public GameObject prefabNetwork;
    public GameObject opcionEquipo, fuegoAmigo;
    public RectTransform todosContraTodos;

    // Start is called before the first frame update
    void Start()
    {
        Menu.lobby = this.GetComponent<Lobby>();

        if (Menu.partidaLocal)
        {
            opcionEquipo.SetActive(false);
            fuegoAmigo.SetActive(false);
            todosContraTodos.offsetMax = new Vector2(400f, todosContraTodos.offsetMax.y);
            Instantiate(prefabNetwork);
            Menu.lobby.CreateGame();
        }
        else
        {
            opcionEquipo.SetActive(true);
            fuegoAmigo.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
