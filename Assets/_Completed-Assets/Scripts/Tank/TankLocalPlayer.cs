using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

/// <summary>
/// Clase que se ocupa de cambiar el color a los tanks
/// </summary>
public class TankLocalPlayer : NetworkBehaviour
{
	/// <summary>
	/// Color por defecto del tanque local
	/// </summary>
	public Color m_LocalTankColor = Color.blue;

	/// <summary>
	/// Color del enemigo por defecto
	/// </summary>
	public Color m_EnemyTankColor = Color.red;

	//Unidad para normalizar
	private const float unit = 0.0039f;

	/// <summary>
	/// Cambia el color al iniciar la partida
	/// </summary>
	private void Start()
	{
		if(isLocalPlayer)
		{
			//Definimos el evento para cambiar de color en tanque
			Menu.cambiarColor = changeColor;

			//Le damos el color por defecto a nuestro tanque local
			cambiarColor(m_LocalTankColor);

			//Asignamos este jugador al controlador de paneles
			ControlPaneles.miJugador = this.gameObject;
		}
		else
		{
			//Le damos el color por defecto a los tanques enemigos
			cambiarColor(m_EnemyTankColor);
		}

		//Agregamos al jugador al array de jugadores
		ControlPaneles.jugadores.Add(this.gameObject);
	}

	/// <summary>
	/// Funcion para evento que cambia el color unicamente si es el jugador local
	/// </summary>
	/// <param name="color"></param>
	private void changeColor(int color)
	{
		if (isLocalPlayer)
		{
			
            switch (color)
            {
				case 1:
					cambiarColor(new Color(1.0f, nor(140.0f), 0.0f,1.0f));
				break;
				case 2:
					cambiarColor(new Color(nor(17.0f), nor(245.0f), nor(208.0f), 1.0f));
				break;
				case 3:
					cambiarColor(new Color(nor(134.0f), nor(248.0f), nor(17.0f), 1.0f));
				break;
				case 4:
					cambiarColor(new Color(1.0f, 0.0f, 1.0f, 1.0f));
				break;
				case 5:
					cambiarColor(new Color(0.0f, nor(128.0f), 1.0f, 1.0f));
				break;
			}
		}
	}

	/// <summary>
	/// Cambia el color de todas las mallas
	/// </summary>
	/// <param name="color"></param>
	public void cambiarColor(Color color)
    {
			foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
			{
				child.material.color = color;
			}
	}

	/// <summary>
	/// Normaliza de 255.0f a 1.0f
	/// </summary>
	/// <param name="value">Valor a normalizar</param>
	/// <returns>Valor normalizado</returns>
	private float nor(float value)
    {
		return (value*unit);
    }
}
