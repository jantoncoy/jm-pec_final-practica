using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayfabController : MonoBehaviour
{
    public static PlayfabController PFC;

    private string userEmail;                   // User's e-mail address
    private string userPassword;                // User's password
    private string username;                    // User's name
    public string puntuacionIndividual = "", puntuacionEquipo = ""; // puntuaciones
    public GameObject loginPanel;               // Login panel reference
	[HideInInspector]public List<FriendInfo> _friends = null;
	[HideInInspector]public int friendIdType;
    private void OnEnable()
    {
        if (PlayfabController.PFC == null)
        {
            PlayfabController.PFC = this;
        }
        else
        {
            if (PlayfabController.PFC != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    public void Start()
    {
        // Note: Setting title Id here can be skipped if you have set the value in Editor Extensions already.
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            PlayFabSettings.staticSettings.TitleId = "C819A"; // Please change this value to your own titleId from PlayFab Game Manager
        }

        if (PlayerPrefs.HasKey("Email"))
        {
			//We get email and password from previous login.
            userEmail = PlayerPrefs.GetString("Email");
            userPassword = PlayerPrefs.GetString("Password");

            var request = new LoginWithEmailAddressRequest { Email = userEmail, Password = userPassword };
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);

            Debug.Log(userEmail + " - Autologin");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Congratulations, you made your first successful API call!");
        GoToLobby();
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.");
        Debug.LogError("Here?s some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        ModalError.mostrarError("Usuario no valido","No se encontro el usuario, vamos a proceder a darlo de alta.", 6);
        var registerRequest = new RegisterPlayFabUserRequest { Email = userEmail, Password = userPassword, Username = username, DisplayName=username };
        PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);
    }

    // Method invoked on user registration failure
    private void OnRegisterFailure(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
        ModalError.mostrarError("Error en registro", "Ocurrio el siguiente error: "+error.GenerateErrorReport(), 8);
    }

    // Method invoked when new user registration is successful
    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        GoToLobby();
		GetFriends();
    }

    private void GoToLobby()
    {
		//Saving on Player Preferences to enable Autologin
        PlayerPrefs.SetString("Email", userEmail);
        PlayerPrefs.SetString("Password", userPassword);
        //GetStats();
        getDisplayName();
       
        //cambiar de escena
        SceneManager.LoadScene("Menu");
    }

    // Sets the user's e-mail address
    public void SetUserEmail(string emailIn)
    {
        userEmail = emailIn;
    }

    // Sets the user's password
    public void SetUserPassword(string passwordIn)
    {
        userPassword = passwordIn;
    }

    // Sets the user's name
    public void SetUserName(string usernameIn)
    {
        username = usernameIn;
    }

    // Invoked when the login button is clicked
    public void OnClickLogin()
    {
        var request = new LoginWithEmailAddressRequest { Email = userEmail, Password = userPassword };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
    }

	public void OnClickLogout()
	{
		PlayFabClientAPI.ForgetAllCredentials();
		PlayerPrefs.DeleteAll();
		SceneManager.LoadScene("Login");
	}

    public void getPuntuacionEquipo()
    {
        puntuacionEquipo = "Cargando...";
        GetLeaderboardRequest peticion = new GetLeaderboardRequest();
        peticion.StartPosition = 0;
        peticion.StatisticName = "equipo";
        PlayFabClientAPI.GetLeaderboard(peticion, getPuntuacioSuccessnEquipo, getPuntuacionError);
    }

    public void getPuntuacionIndividual()
    {
        puntuacionIndividual = "Cargando...";
        GetLeaderboardRequest peticion = new GetLeaderboardRequest();
        peticion.StartPosition = 0;
        peticion.StatisticName = "individual";
        PlayFabClientAPI.GetLeaderboard(peticion, getPuntuacioSuccessnIndividual, getPuntuacionError);
    }

    public void updatePuntuacion(bool esIndividual, int puntuacion)
    {
        string estadistica = "individual";

        if (!esIndividual)
        {
            estadistica = "equipo";
        }

        UpdatePlayerStatisticsRequest peticion = new UpdatePlayerStatisticsRequest();
        StatisticUpdate valor = new StatisticUpdate();
        valor.StatisticName = estadistica;
        valor.Value = puntuacion;
        List<StatisticUpdate> lista = new List<StatisticUpdate>();
        lista.Add(valor);
        peticion.Statistics = lista;
        PlayFabClientAPI.UpdatePlayerStatistics(peticion,UpdatePuntuacionSuccess,getPuntuacionError);
    }

    public void UpdatePuntuacionSuccess(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Se sube satisfactoriamente la puntuacion.");
    }

    public void getPuntuacioSuccessnEquipo(GetLeaderboardResult resultado)
    {
        string equipo = "";

        foreach (PlayerLeaderboardEntry puntuacion in resultado.Leaderboard)
        {
            equipo += "\n" + puntuacion.Position + ". " + puntuacion.DisplayName + " " + puntuacion.StatValue;
        }

        puntuacionEquipo = equipo;
    }

    public void getPuntuacioSuccessnIndividual(GetLeaderboardResult resultado)
    {
        string puntuaciones = "";

        foreach (PlayerLeaderboardEntry puntuacion in resultado.Leaderboard)
        {
            puntuaciones += "\n" + puntuacion.Position + ". " + puntuacion.DisplayName + " " + puntuacion.StatValue;
        }

        puntuacionIndividual = puntuaciones;
    }

    public void getPuntuacionError(PlayFabError error)
    {
        ModalError.mostrarError("Error "+error.Error, "Ocurrio el siguiente error:"+error.ErrorMessage, 6);
    }

    #region PlayerStats

    public string playerDisplayName;                 // Player's level statistic
   // public int playerColor;                   // Game'ls level statistic


    // Set player statistics via client (this method will only work if the "Allow client to post player statistics" is checked
    // on PlayFab Dashboard > Settings > API Features

   public void getDisplayName()
    {
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest(),
        result => {
            if (result.PlayerProfile.DisplayName == null)
            {
                Debug.Log("No PlayerDisplayName");

            }
            else
            {
               
                playerDisplayName = result.PlayerProfile.DisplayName;
                Debug.Log("PlayerDisplayName: " + playerDisplayName);
                PlayerPrefs.SetString("DisplayName", playerDisplayName);
                Debug.Log(PlayerPrefs.GetString("DisplayName"));
                DisplayName.PlayerDisplayName = playerDisplayName;
                Debug.Log(DisplayName.PlayerDisplayName);
            }


        },
       error => {
           ModalError.mostrarError("Error de transferencia", "No se pudo obtener TitleData.", 5);
           Debug.Log("Got error getting titleData:");
           Debug.Log(error.GenerateErrorReport());
       }
   );

    }

    // Method to get player statistics
    public void GetStats()
    {
        //PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest(), OnGetStatistics, error => Debug.LogError(error.GenerateErrorReport()));
       PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
       result => {
           if (result.Data == null || !result.Data.ContainsKey("PlayerDisplayName"))
           {
               Debug.Log("No PlayerDisplayName");
               
           }
           else
           {
               Debug.Log("PlayerDisplayName: " + result.Data["PlayerDisplayName"]);
               playerDisplayName = result.Data["PlayerDisplayName"];
           }
          
       
       },
       error => {
           ModalError.mostrarError("Error de transferencia", "No se pudo obtener TitleData.", 5);
           Debug.Log("Got error getting titleData:");
           Debug.Log(error.GenerateErrorReport());
       }
   );
    }

    // Method invoked when player statistics are received
   /* public void OnGetStatistics(GetPlayerStatisticsResult result)
    {
        Debug.Log("Received the following Statistics:");
        foreach (var eachStat in result.Statistics)
        {
            Debug.Log("Statistic (" + eachStat.StatisticName + "): " + eachStat.Value);
            switch (eachStat.StatisticName)
            {
                case "PlayerDisplayName":
                    playerDisplayName = eachStat.Value;
                    break;
            }
        }
    }*/

	//LISTADO DE FUNCIONES QUE PERMITEN LA CONFIGURACIÓN DE LA LISTA DE AMIGOS

	void DisplayFriends(List<FriendInfo> friendsCache) { friendsCache.ForEach(f => Debug.Log(f.TitleDisplayName)); }
	void DisplayPlayFabError(PlayFabError error) { Debug.Log(error.GenerateErrorReport()); ModalError.mostrarError("Error "+error.Error, "Ocurrio el siguiente error:"+error.ErrorMessage, 6); }
	void DisplayError(string error) { Debug.LogError(error); }

	public void GetFriends() {
	    PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest {
	        IncludeSteamFriends = false,
	        IncludeFacebookFriends = false,
	        XboxToken = null
	    }, result => {
	        _friends = result.Friends;
	        DisplayFriends(_friends);
			Debug.Log(_friends.Count);// triggers your UI
	    }, DisplayPlayFabError);
	}


	public void AddFriend(int FriendIdType, string friendId) 
	{
		var request = new AddFriendRequest();
		switch (FriendIdType) {
		    case 0:
		        request.FriendPlayFabId = friendId;
		        break;
		    case 1:
		        request.FriendUsername = friendId;
		        break;
		    case 2:
		        request.FriendEmail = friendId;
		        break;
		    case 3:
		        request.FriendTitleDisplayName = friendId;
		        break;
	}

    // Execute request and update friends when we are done
    PlayFabClientAPI.AddFriend(request, result => {
        Debug.Log("Friend added successfully!"); ModalError.mostrarError("FRIEND INFO ", "Friend added successfully!", 6); 
    }, DisplayPlayFabError);}

	public void RemoveFriend(FriendInfo friendInfo) 
	{
		PlayFabClientAPI.RemoveFriend(new RemoveFriendRequest {
		    FriendPlayFabId = friendInfo.FriendPlayFabId
		}, result => {
		    _friends.Remove(friendInfo);  ModalError.mostrarError("FRIEND INFO ", "Friend removed successfully.", 6); 
		}, DisplayPlayFabError);
	}


    #endregion PlayerStats

}
