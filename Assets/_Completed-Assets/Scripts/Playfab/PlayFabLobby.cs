using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class PlayFabLobby : MonoBehaviour
{
	[HideInInspector] public PlayfabController playfabController;
    public InputField addFriendField;
	public GameObject FriendNameText;
	private float friendPosition=450f;
	private float refreshList = 2f;

	void Awake()
    {
        playfabController = FindObjectOfType<PlayfabController>();
		playfabController.GetFriends();
    }
	void Start()
	{
		displayFriendsList();
	}

	void Update()
	{
		refreshList+=Time.deltaTime;

		if(refreshList>3f)
		{
			playfabController.GetFriends();	
			displayFriendsList();
			refreshList=0f;
		}
	}

    // Update is called once per frame
	public void ChooseIdType(int friendIdType)
	{
		playfabController.AddFriend(friendIdType,addFriendField.text);
		playfabController.GetFriends();
		displayFriendsList();

	}

	public void displayFriendsList()
	{	
		int index = 0;
		friendPosition=450f;
		DestroyWithTag("Friend");
		Debug.Log(playfabController._friends.Count + "LISTA A INSTANCIAR");
		foreach (var friend in playfabController._friends)
		{
			GameObject FriendName = GameObject.Instantiate(FriendNameText, Vector3.zero, Quaternion.identity,GameObject.Find("FriendListFriends").transform);
			FriendName.transform.position= new Vector3(130f,friendPosition,0);
			FriendName.GetComponent<TextMeshProUGUI>().text=friend.TitleDisplayName;
			FriendName.name = index.ToString();
			friendPosition-=20f;
			index++;
		}
	}
	
	 
    void DestroyWithTag (string destroyTag)
    {
        GameObject[] destroyObject;
        destroyObject = GameObject.FindGameObjectsWithTag(destroyTag);
        foreach (GameObject oneObject in destroyObject)
            Destroy (oneObject);
    }

}
