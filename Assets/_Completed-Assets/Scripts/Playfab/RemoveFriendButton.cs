using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;


public class RemoveFriendButton : MonoBehaviour
{
	[HideInInspector] public PlayfabController playfabController;
	[HideInInspector] public PlayFabLobby playfabLobby;
	void Awake()
    {
        playfabController = FindObjectOfType<PlayfabController>();
        playfabLobby = FindObjectOfType<PlayFabLobby>();
    }

	public void RemoveFriend()
	{	
		Debug.Log("APRETADO BOT�N DE BORRADO");
		int index;
		index = Convert.ToInt32(gameObject.name);
		playfabController.RemoveFriend(playfabController._friends[index]);
		Destroy(this.gameObject);
	}


}
