using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System.Linq;
using TMPro;
namespace Complete
{
	public class GameLocalManager : MonoBehaviour
	{
	    public GameObject m_TankPrefab;             // Reference to the prefab the players will control
	    public LocalTankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks
		public int m_TotalPlayers = 2;					// Variable que indica el n�mero de jugadores que participan en la ronda
	
		private int m_StartingPlayers;				// Variable que contiene el n�mero de jugadores que iniciaron la partida, para evitar que se a�ada un jugador ya a�adido
		private int m_AddedPlayer3 = 0;				// Variable que sirve de control para evitar que se a�ada un jugador ya a�adido
		private int m_AddedPlayer4 = 0;				// Variable que sirve de control para evitar que se a�ada un jugador ya a�adido
        private LocalTankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won

		//PanelResumenLocal
		public GameObject panelResumen;
		public TextMeshProUGUI winsPlayer1;
		public TextMeshProUGUI winsPlayer2;
		public TextMeshProUGUI winsPlayer3;
		public TextMeshProUGUI winsPlayer4;
		public TextMeshProUGUI roundWinnerText;

		private bool m_RoundEnded;

	    private void Start()
	    {
			// Se recoge el n�mero de jugadores iniciales
			m_StartingPlayers = m_TotalPlayers;
			StartGame(m_StartingPlayers);
	    }
	
		private void Update()
		{
			// Modificado para a�adir un nuevo jugador al apretar un bot�n. Se comprueba si el jugador se encuentra en la partida ya y si ya se ha llegado al n�mero m�ximo de jugadores
			AddPlayer3();
			AddPlayer4();

			EndRound();
			UpdatePanelResumen();
	
		}
	
		// Funci�n que inicializa la partida seg�n el n�mero de jugadores
		public void StartGame(int StartingPlayers)
	    { 
			m_TotalPlayers=StartingPlayers;
	
	        SpawnAllTanks();
	    }
	
	
		// Funci�n que a�ade un nuevo jugador a la partida
		public void AddNewPlayer(int PlayerNumber)
		{	
			m_Tanks[PlayerNumber-1].m_Instance.gameObject.SetActive (true);
			m_Tanks[PlayerNumber-1].m_Playing = true;
		}
	
		// Funci�n que a�ade jugador 3 a la partida
		public void AddPlayer3()
		{	
			if(Keyboard.current.vKey.wasPressedThisFrame)
			{
				if(m_TotalPlayers<4 && m_AddedPlayer3 == 0 && m_StartingPlayers<3)
				{
					m_TotalPlayers++;
					AddNewPlayer(3);
					Debug.Log("A�adidoJugador3");
					m_AddedPlayer3 = 1;
				}
			}
		}
	
		// Funci�n que a�ade jugador 4 a la partida
		public void AddPlayer4()
		{	
			if(Keyboard.current.mKey.wasPressedThisFrame)
			{
				if(m_TotalPlayers<4 && m_AddedPlayer4 == 0 && m_StartingPlayers<4)
				{
					m_TotalPlayers++;
					AddNewPlayer(4);
					Debug.Log("A�adidoJugador4");
					m_AddedPlayer4 = 1;
				}
			}
		}
		
		
		// Funci�n modificada con el objetivo de habilitar �nicamente los tanques seleccionados en el men� de selecci�n
		private void SpawnAllTanks()
		{	
			// Se generan los 4 tanques y se deshabilitan
			for (int i = 0; i < m_Tanks.Length; i++)
			{
				// ... create them, set their player number and references needed for control
				m_Tanks[i].m_Instance =
					Instantiate (m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
				m_Tanks[i].m_PlayerNumber = i + 1;
				m_Tanks[i].Setup();
				m_Tanks[i].m_Instance.gameObject.SetActive (false);
			}
			
			// Se activa el n�mero de tanques indicado en el men� de selecci�n
			for (int i = 0; i < m_TotalPlayers; i++)
			{
				m_Tanks[i].m_Instance.gameObject.SetActive (true);
				m_Tanks[i].m_Playing = true;
			}
		}

		public void ResetAllTanks()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
				// �nicamente sobre los tanques activos
				if(m_Tanks[i].m_Playing)
				{
					m_Tanks[i].Reset();
				}
            }

			m_RoundEnded = false;
        }

		private LocalTankManager GetRoundWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
				// �nicamente sobre los tanques activos			
				if(m_Tanks[i].m_Playing)
				{
					// ... and if one of them is active, it is the winner so return it
					if (m_Tanks[i].m_Instance.activeSelf)
					{
					    return m_Tanks[i];
					}
				}
            }

            // If none of the tanks are active it is a draw so return null
            return null;
        }

		// This is used to check if there is one or fewer tanks remaining and thus the round should end
        private bool OneTankLeft()
        {
            // Start the count of tanks left at zero
            int numTanksLeft = 0;

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
				// �nicamente sobre los tanques activos
				if(m_Tanks[i].m_Playing)
				{
					// ... and if they are active, increment the counter
					if (m_Tanks[i].m_Instance.activeSelf)
					{
					  numTanksLeft++;
					}
				}
            }

            // If there are one or fewer tanks remaining return true, otherwise return false
            return numTanksLeft <= 1;
        }

		private void EndRound()
		{
			if(OneTankLeft()&& !m_RoundEnded)
			{
				m_RoundWinner = GetRoundWinner(); 
				m_RoundWinner.m_Wins++;
				roundWinnerText.text = ("Player "+m_RoundWinner.m_PlayerNumber + " wins!").ToString();
				Invoke("ResetAllTanks",3f);
				activarPanelResumen();
				Invoke("desactivarPanelResumen",4f);
				m_RoundEnded = true;
			}
		}

		private void activarPanelResumen()
		{
		    panelResumen.SetActive(true);
		}

		/// <summary>
		/// Desactiva el panel resumen
		/// </summary>
		private void desactivarPanelResumen()
		{
		    panelResumen.SetActive(false);
		}

		private void UpdatePanelResumen()
		{
			winsPlayer1.text=(m_Tanks[0].m_Wins).ToString();
			winsPlayer2.text=(m_Tanks[1].m_Wins).ToString();
			winsPlayer3.text=(m_Tanks[2].m_Wins).ToString();
			winsPlayer4.text=(m_Tanks[3].m_Wins).ToString();
		}
	}
}