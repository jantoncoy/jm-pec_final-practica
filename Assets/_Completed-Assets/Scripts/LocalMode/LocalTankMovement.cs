﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Complete
{
    public class LocalTankMovement : MonoBehaviour
    {
        public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player. This is set by this tank's manager
        public float m_Speed = 12f;                 // How fast the tank moves forward and back
        public float m_TurnSpeed = 180f;            // How fast the tank turns in degrees per second
        public AudioSource m_MovementAudio;         // Reference to the audio source used to play engine sounds. NB: different to the shooting audio source
        public AudioClip m_EngineIdling;            // Audio to play when the tank isn't moving
        public AudioClip m_EngineDriving;           // Audio to play when the tank is moving
		public float m_PitchRange = 0.2f;           // The amount by which the pitch of the engine noises can vary


        private string m_MovementAxisName;          // The name of the input axis for moving forward and back
        private string m_TurnAxisName;              // The name of the input axis for turning
        private Rigidbody m_Rigidbody;              // Reference used to move the tank
        private float m_OriginalPitch;              // The pitch of the audio source at the start of the scene
		
		private PlayerInput m_playerInput;			// Se introduce el New Input System
		private Vector2 m_newMovement;				// Vector que recoge el valor del movimiento a través del New Input System

        private void Awake()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
			m_playerInput= GetComponent<PlayerInput>();
        }

        private void Start()
        {
            // Store the original pitch of the audio source
            m_OriginalPitch = m_MovementAudio.pitch;
        }

        private void Update ()
        {
            EngineAudio();

			// Se modifica el Control Scheme según el número de jugador
			m_playerInput.SwitchCurrentControlScheme("KeyboardPlayer"+m_PlayerNumber, Keyboard.current);

        }

        private void FixedUpdate()
        {
            // Adjust the rigidbodies position and orientation in FixedUpdate.
            Move();
            Turn();
        }

		// Función dónde se obtiene los valores del New Input System
		private void OnMove(InputValue value)
		{
			m_newMovement= value.Get<Vector2>();
		}

		// Función modificada para adaptarse al New Input System. Se hace uso del vector m_newMovement.
		private void Move()
        {
            // Create a vector in the direction the tank is facing with a magnitude based on the input, speed and the time between frames
            Vector3 movement = transform.forward * m_newMovement.y * m_Speed * Time.deltaTime;

            // Apply this movement to the rigidbody's position
            m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
        }

		// Función modificada para adaptarse al New Input System. Se hace uso del vector m_newMovement.
        private void Turn()
        {
            // Determine the number of degrees to be turned based on the input, speed and time between frames
            float turn = m_newMovement.x * m_TurnSpeed * Time.deltaTime;

            // Make this into a rotation in the y axis
            Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);

            // Apply this rotation to the rigidbody's rotation
            m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
        }

		// Función modificada para adaptarse al New Input System. Se hace uso del vector m_newMovement.
        private void OnEnable()
        {
            // When the tank is turned on, make sure it's not kinematic
            m_Rigidbody.isKinematic = false;

            // Also reset the input values
            m_newMovement = Vector2.zero;
        }


        private void OnDisable()
        {
            // When the tank is turned off, set it to kinematic so it stops moving
            m_Rigidbody.isKinematic = true;
        }

		// Función modificada para adaptarse al New Input System. Se hace uso del vector m_newMovement.
        private void EngineAudio()
        {
            // If there is no input (the tank is stationary)...
            if (m_newMovement.magnitude < 0.1f)
            {
                // ... and if the audio source is currently playing the driving clip...
                if (m_MovementAudio.clip == m_EngineDriving)
                {
                    // ... change the clip to idling and play it
                    m_MovementAudio.clip = m_EngineIdling;
                    m_MovementAudio.pitch = Random.Range (m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play ();
                }
            }
            else
            {
                // Otherwise if the tank is moving and if the idling clip is currently playing...
                if (m_MovementAudio.clip == m_EngineIdling)
                {
                    // ... change the clip to driving and play
                    m_MovementAudio.clip = m_EngineDriving;
                    m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                    m_MovementAudio.Play();
                }
            }
        }
    }
}