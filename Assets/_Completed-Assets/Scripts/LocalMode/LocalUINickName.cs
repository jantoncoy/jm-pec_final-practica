using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;

/// <summary>
/// Clase encargada de gestionar los nombres de los jugadores
/// </summary>
namespace Complete
{
	public class LocalUINickName : MonoBehaviour
	{
		[HideInInspector]
	    public string m_NicknameText;                      
	
		public int m_PlayerNumber = 1;              // Used to identify which tank belongs to which player. This is set by this tank's manager
		private TextMeshProUGUI  m_Input;
		private TMP_InputField  m_InputField;
		public TextMeshPro m_Nickname;
	
	    // Start is called before the first frame update
	    void Start()
	    {	
			m_Nickname.text = ("Player " + m_PlayerNumber).ToString();
		}
	}
}


