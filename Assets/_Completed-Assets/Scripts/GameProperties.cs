using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class GameProperties : NetworkBehaviour
{
    [SyncVar]
    public bool teamGame=false;

    [Command]
    public void setGameMode(bool teamGame)
    {

        this.teamGame = teamGame;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (isServer)
        {
            print("Soy Server");
            if (!Menu.partidaTodos)
            {
                //setGameMode(true);
                teamGame = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
