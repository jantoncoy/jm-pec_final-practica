using Complete;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gestor que se ocupa de administrar, crear y borrar los enemigos
/// </summary>
public class EnemyManager : NetworkBehaviour
{
    public  int MAXENEMIGOS = 4;          // Indica los enemigos maximos en partida

    private bool isServer = false;              // Indica si la instancia es el servidor o creador de la partida, Indicate if the instance is server or creator of match
    
    public List<Transform> ruta1;                 //Ruta 1 de los enemigos
    public List<Transform> ruta2;                 //Ruta 2 de los enemigos
    public List<Transform> ruta3;                 //Ruta 3 de los enemigos

    public Transform [] zonasSpawn;             //Zonas de spawn de los enemigos
    public Transform[] zonasMina;               //Zonas de spawn de las minas
    public Transform[] zonasCura;               //Zonas de spawn de las curas
    public Transform zonaBoss;

    public GameObject prefabBoss;               //Prefab del Boss
    public GameObject prefabEnemigo;            //Prefab del enemigo
    public GameObject prefabMina;               //Prefab de la mina
    public GameObject prefabHealth;             //Prefab del objeto cura

    public List<GameObject> enemigos;           //Array de enemigos
    public List<GameObject> minas;              //Array de minas
    public List<GameObject> curas;              //Array de curas
    public GameObject boss;                     //Boss 

    public static Action finalRonda;            //Se llama cuando se reinicia la ronda, contiene la llamada al metodo interno final

    // Start is called before the first frame update
    void Start()
    {
		if (!Menu.partidaLocal)
		{
			isServer = !isLocalPlayer;

			//Si es el servidor instanciamos los enemigos
			if (isServer)
			{
			    //Inicializamos el array
			    enemigos = new List<GameObject>();

			    //El primer enemigo lo generamos a los dos segundos, los siguientes cada 4
			    InvokeRepeating("instanciarEnemigo", 1, 4);
			    InvokeRepeating("instanciarMinas", 1, 1);
                InvokeRepeating("instanciarVida", 1, 4);
                Invoke("instanciarBoss", 8);

                //Asignamos el evento
                finalRonda = final;
			}
			else
			{
			    finalRonda = null;
			}
		}

    }

    /// <summary>
    /// Instanciamos un enemigo
    /// </summary>
    public void instanciarEnemigo()
    {
        //Si no llegamos al maximo de enemigos y es el servidor instanciamos el enemigo
        if(enemigos.Count != MAXENEMIGOS)
        {
            if (isServer)
            {
                //Escogemos posicion y zona aleatoria
                int posicion = UnityEngine.Random.Range(0, zonasSpawn.Length);
                int zona = UnityEngine.Random.Range(1, 4);

                //Instanciamos el enemigo
                GameObject enemigo = Instantiate(prefabEnemigo, zonasSpawn[posicion].position, prefabEnemigo.transform.rotation) as GameObject;
                enemigo.GetComponent<TankMovement>().isIA = true;

                //Escogemos zona
                if (zona == 1)
                    enemigo.GetComponent<TankMovement>().waypoints = ruta1;
                if (zona == 2)
                    enemigo.GetComponent<TankMovement>().waypoints = ruta2;
                if (zona == 3)
                    enemigo.GetComponent<TankMovement>().waypoints = ruta3;
                
                //Agregamos el array a los enemigos
                enemigos.Add(enemigo);

                NetworkServer.Spawn(enemigo);
            }
        }
        else
        {
            //Cuando llega al maximo paramos de generar enemigos
            CancelInvoke("instanciarEnemigo");
        }
    }

    /// <summary>
    /// Instanciamos las minas en posiciones random
    /// </summary>
    private void instanciarMinas()
    {
        //Si no llegamos al maximo de minas y es el servidor instanciamos las minas
        if (minas.Count != 6)
        {
            if (isServer)
            {
                int posicion = UnityEngine.Random.Range(0, zonasMina.Length);
                GameObject mina = Instantiate(prefabMina, zonasMina[posicion].position, prefabMina.transform.rotation) as GameObject;

                //Agregamos el array a los enemigos
                minas.Add(mina);

                NetworkServer.Spawn(mina);
            }
        }
        else
        {
            //Cuando llega al maximo paramos de generar enemigos
            CancelInvoke("instanciarMinas");
        }

    }

    /// <summary>
    /// Instanciamos las curas en posiciones random
    /// </summary>
    private void instanciarVida()
    {
        //Si no llegamos al maximo de minas y es el servidor instanciamos las minas
        if (curas.Count != 8)
        {
            if (isServer)
            {
                int posicion = UnityEngine.Random.Range(0, zonasCura.Length);
                GameObject cura = Instantiate(prefabHealth, zonasCura[posicion].position, prefabHealth.transform.rotation) as GameObject;

                //Agregamos el array a los enemigos
                curas.Add(cura);

                NetworkServer.Spawn(cura);
            }
        }
        else
        {
            //Cuando llega al maximo paramos de generar vida
            CancelInvoke("instanciarVida");
        }

    }


    /// <summary>
    /// Destruye a los enemigos y minas,luego los vuelve a crear
    /// </summary>
    private void final()
    {
        if (isServer)
        {
            //Destruimos los enemigos
            foreach(GameObject enemigo in enemigos)
                NetworkServer.Destroy(enemigo);
            
            //Vaciamos array y lo incializamos
            enemigos = new List<GameObject>();

            //Destruimos los enemigos
            foreach (GameObject mina in minas)
                NetworkServer.Destroy(mina);

            //Vaciamos array y lo incializamos
            minas = new List<GameObject>();

            //Destruimos el boss
            NetworkServer.Destroy(boss);
            boss = null;

            //Volvemos a generar los enemigos
            InvokeRepeating("instanciarEnemigo", 1, 4);
            InvokeRepeating("instanciarMinas", 1, 2);
            InvokeRepeating("instanciarVida", 1, 4);
            Invoke("instanciarBoss", 8);
        }
    }

    /// <summary>
    /// Instanciamos un boss
    /// </summary>
    public void instanciarBoss()
    {
        if (isServer)
        {
            //Instanciamos el boss
            GameObject boss = Instantiate(prefabBoss, zonaBoss.position, prefabBoss.transform.rotation) as GameObject;
            boss.GetComponent<TankMovement>().isIA = true;

            //Guardamos el bos
            this.boss = boss;

            NetworkServer.Spawn(boss);
        }
    }
}
