using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script que contiene la configuracion del lobby al presionar los botones
/// </summary>
public class Lobby : MonoBehaviour 
{
    private NetworkManager manager;
    public string serverIP = "localhost";
    public InputField newIPField;

    /// <summary>
    /// Cuando se activa recoge el manager
    /// </summary>
    void Awake(){
        actualizarManager();
    }

    /// <summary>
    /// actualizamos instancia de NetworkManager
    /// </summary>
    public void actualizarManager()
    {
        manager = FindObjectOfType<NetworkManager>();
    }

    /// <summary>
    /// Crea una partida nueva y se une
    /// </summary>
    public void CreateGame() 
	{
        actualizarManager();

        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartHost();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Se une a una partida
    /// </summary>
    public void JoinGame() 
	{
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.networkAddress = serverIP;
                manager.StartClient();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Inicia unicamente el servidor
    /// </summary>
    public void CreateServer() 
	{
        if (!NetworkClient.isConnected && !NetworkServer.active) {
            if (!NetworkClient.active) {
                manager.StartServer();
            }
        }

        ServerData();
    }

    /// <summary>
    /// Muestra informacion
    /// </summary>
    private void ServerData() 
	{
		ServerJoined();
        Debug.Log ("Local IP Address: " + GetLocalIP() + "\r\n ------------------------------------------------------------------------");
    }

    /// <summary>
    /// devuelve la ip 
    /// </summary>
    /// <returns></returns>
	private string GetLocalIP() 
	{
		var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
		foreach (var ip in host.AddressList) 
		{
			if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) 
			{
			    return ip.ToString();
			}
		}

        ModalError.mostrarError("Error conectando al servidor", "No se encuentra adaptador de red con IPv4 en el sistema.", 6);
        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }

    /// <summary>
    /// Muestra informacion del servidor
    /// </summary>
	private void ServerJoined()
	{
		if (NetworkServer.active) 
		{
            Debug.Log ("Server IP: " + manager.networkAddress + " / Transport: " + Transport.activeTransport);
        }
        else
		{
            ModalError.mostrarError("Error conectando al servidor","No se puede conectar al servidor con la ip: " +serverIP+", compruebe la ip y sus puertos.",10);
            Debug.Log ("Could not join server: " + serverIP);
        }
	}

    /// <summary>
    /// Cambiar IP de busqueda
    /// </summary>
    public void UpdateIP()
    {
        serverIP = newIPField.text;
    }

	/// <summary>
    /// Bot�n crea una partida
    /// </summary>
    public void CreateMatchButton() 
	{
		SceneManager.LoadScene("ModoJuego");
    }

}
