using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamButtons : MonoBehaviour
{
    public GameObject teamCanvas;
    public Text text;
    public Image colorPanel;
	private bool spawnFree;
    // Start is called before the first frame update
    void Start()
    {
        text.text = "Selecciona un Equipo";
        colorPanel.color = new Color32(255, 128, 128, 100);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void joinBlue()
    {
        ControlPaneles.miJugador.GetComponent<Complete.TankHealth>().changeTeam(1);
        text.text = "Equipo Azul";
        colorPanel.color = new Color32(0,0,255,100);
		SpawnBlue(ControlPaneles.miJugador);
    }
    public void joinGreen()
    {
        ControlPaneles.miJugador.GetComponent<Complete.TankHealth>().changeTeam(0);
        text.text = "Equipo Verde";
        colorPanel.color = new Color32(0, 255, 0, 100);
		SpawnGreen(ControlPaneles.miJugador);

		
    }

	private void SpawnGreen(GameObject playersTank)
	{
		GameObject[] GreenSpawns = GameObject.FindGameObjectsWithTag("GreenSpawn");
		GameObject[] Tanks= GameObject.FindGameObjectsWithTag("Player");

		foreach(var spawn in GreenSpawns)
		{
			spawnFree=true;
			foreach(var tank in Tanks)
			{
				if(tank.transform.position==spawn.transform.position)
				{
					spawnFree=false;
				}
			}

			if(spawnFree)
			{
				playersTank.transform.position = spawn.transform.position;
				playersTank.transform.rotation = spawn.transform.rotation;
				break;
			}			
		}
	}

	private void SpawnBlue(GameObject playersTank)
	{
		GameObject[] BlueSpawns = GameObject.FindGameObjectsWithTag("BlueSpawn");
		GameObject[] Tanks= GameObject.FindGameObjectsWithTag("Player");

		foreach(var spawn in BlueSpawns)
		{
			spawnFree=true;
			foreach(var tank in Tanks)
			{
				if(tank.transform.position==spawn.transform.position)
				{
					spawnFree=false;
				}
			}

			if(spawnFree)
			{
				playersTank.transform.position = spawn.transform.position;
				playersTank.transform.rotation = spawn.transform.rotation;
				break;
			}			
		}
	}


}
