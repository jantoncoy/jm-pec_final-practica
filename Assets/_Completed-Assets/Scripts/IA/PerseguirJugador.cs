﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System.Collections.Generic;
using Complete;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    /// <summary>
    /// Persigue al jugadores
    /// </summary>
    [Action("Navigation/PerseguirJugador")]
    [Help("Se mueve entre jugadores")]
    public class PerseguirJugador : GOAction
    {
        const float cambiarObjetivo = 45000.0f;

        ///<value>Jugador</value>
        [OutParam("locationPlayer")]
        [Help("locationPlayer")]
        public int locationPlayer;

        ///<value>Jugador</value>
        [OutParam("tiempo")]
        [Help("tiempo")]
        public float tiempo;

        /// <summary>
        /// Ejecucion movimiento
        /// </summary>
        public override void OnStart()
        {
            GameObject [] jugadores =  GameObject.FindGameObjectsWithTag("Player");

            //Agente de navmesh para controlar la entidad
            NavMeshAgent tanke = this.gameObject.GetComponent<NavMeshAgent>();

            tiempo += Time.deltaTime / 0.001f;
           
            if (tiempo >= cambiarObjetivo)
            {
                locationPlayer++;
                tiempo = 0f;
            }

            Debug.Log(tiempo);

            //Iniciamos a 0
            if(locationPlayer < 0 || locationPlayer >= jugadores.Length)
            {
                locationPlayer = 0;
            }

            if(jugadores != null && jugadores.Length > 0)
            {
                //Obtenemos la posicion del jugador
                Transform posicion = jugadores[locationPlayer].transform;

                if (Vector3.Distance(this.gameObject.transform.position,jugadores[locationPlayer].transform.position) <= 14)
                {
                    this.gameObject.transform.LookAt(posicion);
                    tanke.isStopped = true;
                }
                else
                {
                    //lo perseguimos
                    tanke.isStopped = false;
                    tanke.destination = posicion.position;
                    tanke.speed = 6;
                }

            }
        }

        /// <summary>
        /// Devolucion movimiento
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
                return TaskStatus.COMPLETED;
        }
    }
}