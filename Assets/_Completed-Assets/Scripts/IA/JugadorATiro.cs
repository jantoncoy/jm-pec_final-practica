using System.Collections.Generic;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    /// <summary>
    /// Comprobamos si el jugador esta a tiro
    /// </summary>
    [Condition("Perception/JugadorATiro")]
    [Help("Comprobamos si el jugador esta a tiro")]
    public class JugadorATiro : GOCondition
    {
        ///<value>Distancia maxima para considerar que un enemigo esta a tiro</value>
        [InParam("distanciaMaxVision")]
        [Help("Distancia maxima para considerar que un enemigo es visible")]
        public float distanciaMaxVision;

        ///<value>Vector del enemigo</value>
        [OutParam("vectorEnemigo")]
        [Help("Vector del enemigo")]
        public Vector3 vectorEnemigo;

        /// <summary>
        /// Calcula si el enemigo esta cerca
        /// </summary>
        /// <returns>Retorna true si el enemigo esta cerca</returns>
        public override bool Check()
        {
            //Vamos a detectar solo a los jugadores
            int jugadores = LayerMask.NameToLayer("Players");
            int layerMask = (1 << jugadores);

            Transform transformPropio = this.gameObject.transform;

            //Trazamos tres rayos emulando la vista del tanque
            Vector3 vectorZonaMinima = Quaternion.AngleAxis(-15, Vector3.up) * transformPropio.TransformDirection(Vector3.forward);
            Vector3 vectorZonaMaxima = Quaternion.AngleAxis(15, Vector3.up) * transformPropio.TransformDirection(Vector3.forward);
            Vector3 vectorZonaCentral = transformPropio.TransformDirection(Vector3.forward);
            
            //Almacenaremos las colisiones si existen
            RaycastHit hitIzquierda, hitCentral, hitDerecha;

            //Trazamos los rayos
            Physics.Raycast(transformPropio.position, vectorZonaMinima, out hitIzquierda, distanciaMaxVision, layerMask);
            Physics.Raycast(transformPropio.position, vectorZonaCentral, out hitCentral, distanciaMaxVision, layerMask);
            Physics.Raycast(transformPropio.position, vectorZonaMaxima, out hitDerecha, distanciaMaxVision, layerMask);
            
            //Dibujamos en modo debug los rayos de vision
            Debug.DrawRay(transformPropio.position, vectorZonaCentral * distanciaMaxVision, Color.red);
            Debug.DrawRay(transformPropio.position, vectorZonaMaxima * distanciaMaxVision, Color.red);
            Debug.DrawRay(transformPropio.position, vectorZonaMinima * distanciaMaxVision, Color.red);


            //Comprobamos si existe colision
            GameObject enemigo = comprobarColision(hitIzquierda, hitCentral, hitDerecha);

            //Si existe colision devolvemos true y informamos el vector del enemigo
            if(enemigo != null)
            {
                vectorEnemigo = enemigo.transform.position;
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Comprobamos si existe en alguno de los almacenamientos de golpes colision
        /// </summary>
        /// <param name="izq"></param>
        /// <param name="centro"></param>
        /// <param name="der"></param>
        /// <returns></returns>
        private GameObject comprobarColision(RaycastHit izq, RaycastHit centro, RaycastHit der)
        {
            GameObject resultado = null;

            if (centro.collider != null)
            {
                resultado = centro.collider.gameObject;
            }
            else if (izq.collider != null)
            {
                resultado = izq.collider.gameObject;
            }
            else if (der.collider != null)
            {
                resultado = der.collider.gameObject;
            }

            return resultado;
        }
    }
}